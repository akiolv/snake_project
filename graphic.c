#include "graphic.h"
#include "drawer.h"
#include "utils.h"
#include "snake.h"
#include "keyboard.h"
/*
 * startApplication
 *
 * Start the Gtk Application and return it status
 */
int startApplication(int argc, char **argv){

  GtkApplication *app;
  int status;

  /* Create a new GtkApplication instance */
  app = gtk_application_new ("fr.abernard.snake", G_APPLICATION_FLAGS_NONE);

  /* Create the windows */
  g_signal_connect(app, "activate", G_CALLBACK(activate), NULL);

  /* Runs the application */
  status = g_application_run(G_APPLICATION(app), argc, argv);

  /* Decreases the reference count of the app.
     When its reference count drops to 0, the object is finalized
     (i.e. its memory is freed) */
  g_object_unref(app);

  return status;
}

/*
 * activate
 *
 * Initialisation of the window of the app
 */
void activate(GtkApplication *app, gpointer user_data)
{
  GtkWidget * window;
  GtkWidget * drawingArea;


  /* Create the window related to the app */
  window = gtk_application_window_new(app);
  game.window = window;


  /* Set window configuration*/
  gtk_window_set_title(GTK_WINDOW (window), WINDOW_TITLE);
  gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
  gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);


  /* Call closeWindow function when the window is destroy */
  g_signal_connect(window, "destroy", G_CALLBACK (closeWindow), NULL);


  /* Create the drawing area with an explicit size */
  drawingArea = gtk_drawing_area_new();
  game.drawingArea = drawingArea;
  gtk_widget_set_size_request(drawingArea, WINDOW_WIDTH_DEFAULT, WINDOW_HEIGTH_DEFAULT);


  /* Add the drawing area to the window */
  gtk_container_add(GTK_CONTAINER (window), drawingArea);


  /* Configure and draw the surface */
  g_signal_connect(drawingArea, "draw", G_CALLBACK(drawSurface), NULL);
  g_signal_connect(drawingArea, "configure-event", G_CALLBACK(configureEventSurface), NULL);


  /* Tell gtk to draw the menu line */
  g_signal_connect(drawingArea, "draw", G_CALLBACK(DrawMenuLine), NULL);


  /* Tell gtk to draw the treshold of the game */
  g_signal_connect(drawingArea, "draw", G_CALLBACK(DrawGameThreshold), NULL);


  /* Subscribe drawingArea to the event key mask */
  gtk_widget_add_events(drawingArea, GDK_CONTROL_MASK | GDK_KEY_PRESS_MASK | GDK_POINTER_MOTION_MASK);


  /* Add keyboard event */
  g_signal_connect(window, "key_press_event", G_CALLBACK(onKeyEvent), NULL);


  /* Recursively shows the window, and any window child 
     (because it's a container) */
  gtk_widget_show_all(window);
}


/*
 * closeWindow
 *
 * Function call when the window is destroy
 */
void closeWindow(void)
{
  if(game.surface)
    cairo_surface_destroy(game.surface);
}