#include <gtk/gtk.h>

#ifndef GRAPHIC_H_
#define GRAPHIC_H_

int startApplication(int argc, char **argv);

void activate(GtkApplication *app, gpointer user_data);

void closeWindow(void);

#endif