#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
#include "drawer.h"
#include "utils.h"
#include "snake.h"
#include "constraints.h"

/*
 * onKeyEvent
 *
 * Controller of event key for the game and the menu
 * 		- Enter: start the game
 *      - space: play/pause the game
 * 		- G/g: draw the grid of the game
 *      - F/f: Refresh the game (for debug)
 *      - C/c: Clear the surface (for debug)
 *      - R/r: Restart the game (for debug)
 */
gboolean onKeyEvent(GtkWidget *widget, GdkEventKey * event, gpointer user_data)
{
	int res;
	int fsx;
	int fsy;

	if(game.surface == NULL)
		return FALSE;

	switch (event -> keyval)
	{
		// space bar
		case GDK_KEY_space:
			playEvent();
			break;

		// Enter
		case GDK_KEY_Return:
			startGameEvent();
			break;

		// F/f
		case GDK_KEY_F:
		case GDK_KEY_f:
			printf("[INFO] Refresh the window\n");
			DrawGame();
			break;

		// R/r
		case GDK_KEY_R:
		case GDK_KEY_r:
			printf("[INFO] Restart the game\n");
			clearSurface();
			freeSnake(game.snake);
			game.snake = NULL;
			free(game.fruit);
			game.fruit = NULL;
			startGameEvent();
			break;

		// C/c
		case GDK_KEY_C:
		case GDK_KEY_c:
			printf("[INFO] Clear surface\n");
			clearSurface();
			gtk_widget_queue_draw(game.drawingArea);
			break;

		// Z/z or Up input
		case GDK_KEY_Z:		
		case GDK_KEY_z:
		case GDK_KEY_Up:
			printf("[INFO] Snake up\n");

			fsx = game.snake -> first -> x;
			fsy = game.snake -> first -> y - 1;
			res = checkConstraint(fsx, fsy);

			if(res == 0)
				snakeDeplacement(game.snake, 0, 0);
			if(res == 2)
				snakeDeplacement(game.snake, 1, 0);

			DrawGame();
			break;

		// S/s or Down input
		case GDK_KEY_S:		
		case GDK_KEY_s:
		case GDK_KEY_Down:
			printf("[INFO] Snake down\n");

			fsx = game.snake -> first -> x;
			fsy = game.snake -> first -> y + 1;
			res = checkConstraint(fsx, fsy);

			if(res == 0)
				snakeDeplacement(game.snake, 0, 2);
			if(res == 2)
				snakeDeplacement(game.snake, 1, 2);

			DrawGame();
			break;

		// Q/q or Left input
		case GDK_KEY_Q:		
		case GDK_KEY_q:
		case GDK_KEY_Left:
			printf("[INFO] Snake left\n");

			fsx = game.snake -> first -> x - 1;
			fsy = game.snake -> first -> y;
			res = checkConstraint(fsx, fsy);

			if(res == 0)
				snakeDeplacement(game.snake, 0, 3);
			if(res == 2)
				snakeDeplacement(game.snake, 1, 3);

			DrawGame();
			break;

		// D/d or Right input
		case GDK_KEY_D:		
		case GDK_KEY_d:
		case GDK_KEY_Right:
			printf("[INFO] Snake right\n");

			fsx = game.snake -> first -> x + 1;
			fsy = game.snake -> first -> y;
			res = checkConstraint(fsx, fsy);

			if(res == 0)
				snakeDeplacement(game.snake, 0, 1);
			if(res == 2)
				snakeDeplacement(game.snake, 1, 1);
			
			DrawGame();
			break;

		// G/g
		case GDK_KEY_G:		
		case GDK_KEY_g:			
			showGridEvent();
			break;

		default:
			return FALSE;
	}

	return TRUE;
}



/*
 * playEvent
 *
 * Put the game in pause if the game running
 * and if the game is paused, it will resume
 */
void playEvent()
{
	printf("[INFO] Pause/play\n");

	if(game.play == 0){
		game.play = 1;
	} else {
		game.play = 0;
	}
}


/*
 * startGameEvent
 *
 * Start the game with the initialisation of 
 * the snake and the fruit. This function clear the surface 
 * and draw the snake, the fruit and the grid if the user
 * enable it.
 * This function does nothing if a snake has been already 
 * created
 */
void startGameEvent()
{
	printf("[INFO] Start the game\n");

	if(game.snake == NULL){

		/* Init the snake and the first fruit of the game */
		game.snake = initSnake();
		game.fruit = initFruit();


		/* Clear the surface */
		clearSurface();

		/* Draw the grid if it's enable */
		if(game.showGrid == 1){
			DrawGameGrid(game.drawingArea);
		}

		/* Draw the snake */
		DrawSnake(game.drawingArea);

		/* Draw the fruit */
		DrawFruit(game.drawingArea);

		/* Draw the menu */
		DrawMenu(game.drawingArea);

		/* Show the new surface */
		gtk_widget_queue_draw(game.drawingArea);

	} else {
		printf("[INFO] The game already started \n");
	}
}


/*
 * showGridEvent
 *
 * Show the grid. If the grid is already shown,
 * we clear the surface and draw it again
 */
void showGridEvent()
{
	printf("[INFO] Draw the game grid\n");
	if(game.showGrid == 0)
	{
		game.showGrid = 1;
		DrawGameGrid(game.drawingArea);
		gtk_widget_queue_draw(game.drawingArea);
	} 
	else 
	{
		game.showGrid = 0;

		clearSurface();

		if(game.snake != NULL){

			/* Draw the snake */
			DrawSnake(game.drawingArea);

			/* Draw the menu */
			DrawMenu(game.drawingArea);
		}
		
		if(game.fruit != NULL){

			/* Draw the fruit */
			DrawFruit(game.drawingArea);
		}

		gtk_widget_queue_draw(game.drawingArea);
	}
}