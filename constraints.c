#include "utils.h"
#include "snake.h"


/*
 * checkConstraint()
 *
 * Check where the future head (sx, sy) will be positionned and 
 * return a int if:
 *   - -1: Error when try to find the constraint
 *   - 0: it's a free pixel where the snake can move
 *   - 1: the snake reach a wall
 *   - 2: the snake reach the fruit
 *   - 3: snake eat himself
 *
 */
int checkConstraint(int sx, int sy){

	Snake * snake = game.snake;

	if(snake == NULL){
		printf("[ERROR] Unable to check the constraint on the snake\n");
		return -1;
	}


	Fruit * fruit = game.fruit;

	if(fruit == NULL){
		printf("[ERROR] Unable to check the fruit position\n");
		return -1;
	}


	/* Check if the user reach the fruit */
	if(sx == fruit -> x && sy == fruit -> y){
		printf("[INFO] The snake reach the fruit\n");
		changeFruitPosition(game.snake, game.fruit);
		return 2;
	}


	/* Check if the user reach the wall */
	if(!(sx > 0 && sx < game.nbFakePixel - 1)){
		printf("[INFO] The snake reach the wall %d\n", sx);
		return 1;
	}
	if(!(sy > 0 && sy < game.nbFakePixel - 1)){
		printf("[INFO] The snake reach the wall\n");
		return 1;
	}


	/* Check if the snake eat himself */
	sNode * node = snake -> first -> next;

	for (int i = 2; i < snake -> length - 1; i++){
		node = node -> next;
		if(sx == node -> x && sy == node -> y){
			printf("[INFO] The snake eat himself\n");
			return 3;
		}
	}

	return 0;
}