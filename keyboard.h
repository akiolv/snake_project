#include <gtk/gtk.h>

#ifndef KEYBOARD
#define KEYBOARD

gboolean onKeyEvent(GtkWidget *widget, GdkEventKey * event, gpointer user_data);

void playEvent();

void startGameEvent();

void showGridEvent();

#endif