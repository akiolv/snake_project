#include "utils.h"
#include "graphic.h"
#include "snake.h"

/*
 * --- Main
 *
 * Program begins here
 */
int main(int argc, char *argv[]){ 

	// Init variable of the game structure
	game.surface = NULL;
	game.nbFakePixel = m;
	game.play = 0;
	game.showGrid = 0;
	game.snake = NULL;
	game.fruit = NULL;

  // Start the application
  int status = startApplication(argc, argv);
  
  return status;
}