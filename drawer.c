#include "drawer.h"
#include "utils.h"
#include "snake.h"
#include "constraints.h"

/*
 * clearSurface
 *
 * Clear the surface in paint all the surface in black.
 */
void clearSurface()
{
  cairo_t *cr;

  cr = cairo_create (game.surface);

  cairo_set_source_rgb (cr, 0, 0, 0);
  cairo_paint (cr);

  // Show the line
  cairo_stroke(cr);
  
  cairo_destroy (cr);
}


/*
 * configureEventSurface
 *
 * Configure the surface event and initialize it with the 
 * clearSuface() function.
 */
gboolean configureEventSurface (GtkWidget *widget, GdkEventConfigure *event, gpointer user_data)
{
  if (game.surface)
    cairo_surface_destroy (game.surface);

  game.surface = gdk_window_create_similar_surface (gtk_widget_get_window (widget),
                                               CAIRO_CONTENT_COLOR,
                                               gtk_widget_get_allocated_width (widget),
                                               gtk_widget_get_allocated_height (widget));

  /* Initialize the surface to black */
  clearSurface ();

  /* We've handled the configure event, no need for further processing. */
  return TRUE;
}


/*
 * drawSurface
 *
 * Redraw the screen from the surface
 */
gboolean drawSurface (GtkWidget *widget, cairo_t *cr, gpointer user_data)
{
  cairo_set_source_surface (cr, game.surface, 0, 0);
  cairo_paint (cr);

  return FALSE;
}


/*
 * DrawMenuLine
 *
 * Draw the white menu line when the window is display to the user.
 */
void DrawMenuLine(GtkWidget *widget, cairo_t *cr, gpointer user_data){

	// Set the line color and line width
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_set_line_width(cr, LINE_WIDTH);


	// to draw, need to move the cursor at the start of the line
	// add LINE_WIDTH / 2 to have a quare on the left for the snake
	cairo_move_to(cr, WINDOW_HEIGTH_DEFAULT + (LINE_WIDTH / 2), 0);


	// Draw the line from the init position of the cursor to the next point
	cairo_line_to(cr, WINDOW_HEIGTH_DEFAULT + (LINE_WIDTH / 2), WINDOW_HEIGTH_DEFAULT);


	// Show the line
	cairo_stroke(cr);
}


/*
 * DrawGameThreshold
 *
 * Draw the threshold in the game, depending on the selected number of
 * fake pixel in the game
 */
void DrawGameThreshold(GtkWidget *widget, cairo_t *cr, gpointer user_data){

	// Set the size of the fake pixel for the m size
	int f_pixel = WINDOW_HEIGTH_DEFAULT / game.nbFakePixel;
	int f_half_pixel = f_pixel / 2;

	// Set the line color and line width
	cairo_set_source_rgba(cr, 0.18, 0.8, 0.44, 1.0);
	cairo_set_line_width(cr, f_pixel);

	// Move the cursor to the left-top corner
	cairo_move_to(cr, 0, f_half_pixel);


	// Draw the 4 lines
	cairo_line_to(cr, WINDOW_HEIGTH_DEFAULT - f_half_pixel, f_half_pixel);
	cairo_line_to(cr, WINDOW_HEIGTH_DEFAULT - f_half_pixel, WINDOW_HEIGTH_DEFAULT - f_half_pixel);
	cairo_line_to(cr, f_half_pixel, WINDOW_HEIGTH_DEFAULT - f_half_pixel);
	cairo_line_to(cr, f_half_pixel, 0);

	// Show the line
	cairo_stroke(cr);
}


/*
 * DrawGameGrid
 *
 * Draw the grid for f_pixel (test function)
 */
void DrawGameGrid(GtkWidget *widget){

	cairo_t *cr;

	/* Paint to the surface, where we store our state */
	cr = cairo_create(game.surface);

	// Set the size of the fake pixel for the m size
	int f_pixel = WINDOW_HEIGTH_DEFAULT / game.nbFakePixel;

	// Set the line color and line width
	cairo_set_source_rgb(cr, 1, 1, 1);
	cairo_set_line_width(cr, 1);

	for(int i = 1; i < m; i++){
		cairo_move_to(cr, f_pixel * i, 0);
		cairo_line_to(cr, f_pixel * i, WINDOW_HEIGTH_DEFAULT);

		cairo_move_to(cr, 0, f_pixel * i);
		cairo_line_to(cr, WINDOW_HEIGTH_DEFAULT, f_pixel * i);
	}

	// Show the line
	cairo_stroke(cr);

	// Destroy the cairo object
	cairo_destroy(cr);
}


/*
 * DrawMenu
 *
 * Actually just show the score of the partie
 */
void DrawMenu(GtkWidget *widget){

	cairo_t *cr;

	/* Paint to the surface, where we store our state */
	cr = cairo_create(game.surface);

	/* Move the gtk cursor */
	cairo_move_to(cr, WINDOW_HEIGTH_DEFAULT + 25, 100);

	/* Set the text color */
	cairo_set_source_rgb(cr, 1, 1, 1);	

	/* Set the font size */
	cairo_set_font_size(cr, 18.0);

	/* Set the text */
	char str[30];
	sprintf(str, "Score: %d", game.snake -> length - 2); 

	/* Draw the text */
	cairo_show_text(cr, str);

	// Show the line
	cairo_stroke(cr);

	// Destroy the cairo object
	cairo_destroy(cr);
}

/*
 * DrawPixel
 *
 * Draw a pixel on the screen,
 * at the given position with given fake pixel length.
 */

void DrawPixel(GtkWidget *widget, cairo_t * cr, int x0, int y0, int fpixel){

	cairo_rectangle(cr, x0 * fpixel, y0 * fpixel, fpixel, fpixel);
	cairo_fill(cr);
}


/*
 * DrawSnake
 *
 * Draw the snake on the screen,
 */
void DrawSnake(GtkWidget *widget){

	// Get the snake struct pointer
	Snake * snake = game.snake;
	
	if(snake == NULL){
		printf("[ERROR] Failed to load the snake\n");
		exit(EXIT_FAILURE);
	}

	cairo_t *cr;

	/* Paint to the surface, where we store our state */
	cr = cairo_create(game.surface);


	// Set the size of the fake pixel for the m size
	int f_pixel = WINDOW_HEIGTH_DEFAULT / game.nbFakePixel;


	// Set the color of the snake
	cairo_set_source_rgb(cr, 0.6, 0.34, 0.71);


	// Find the first sNode of the snake
	sNode * node = snake -> first;

	if(node == NULL){
		printf("[ERROR] Failed to load the first node pointer of the snake\n");
		exit(EXIT_FAILURE);	
	}


	// Draw the snake
	for(int i = 0; i < snake -> length; i++){

		DrawPixel(widget, cr, node -> x, node -> y, f_pixel);
		node = node -> next;
	}


	// Show the line
	cairo_stroke(cr);

	// Destroy the cairo object
	cairo_destroy(cr);
}


/*
 * DrawFruit
 *
 * Draw the fruit on the screen,
 */
void DrawFruit(GtkWidget *widget){

	// Get the fruit struct pointer
	Fruit * fruit = game.fruit;
	
	if(fruit == NULL){
		printf("[ERROR] Failed to load the fruit\n");
	}

	cairo_t *cr;

	/* Paint to the surface, where we store our state */
	cr = cairo_create(game.surface);

	// Set the size of the fake pixel for the m size
	int f_pixel = WINDOW_HEIGTH_DEFAULT / game.nbFakePixel;

	// Set the color of the fruit
	cairo_set_source_rgb(cr, 0.91, 0.30, 0.24);


	// Draw the fruit
	DrawPixel(widget, cr, fruit -> x, fruit -> y, f_pixel);

	// Show the line
	cairo_stroke(cr);

	// Destroy the cairo object
	cairo_destroy(cr);
}

/*
 * DrawGame
 *
 * Draw the game if the snake, the fruit and 
 * the grid if it's enable.
 */
void DrawGame(){

	if(game.drawingArea == NULL){
		printf("[ERROR] Unable to load the area to draw the game\n");
		exit(EXIT_FAILURE);		
	}

	if(game.snake == NULL){
		printf("[ERROR] Failed to load the snake\n");
		exit(EXIT_FAILURE);
	}

	
	if(game.fruit == NULL){
		printf("[ERROR] Failed to load the fruit\n");
		exit(EXIT_FAILURE);
	}


	/* Clear the surface */
	clearSurface();

	/* Draw the grid if it's enable */
	if(game.showGrid == 1){
		DrawGameGrid(game.drawingArea);
	}

	/* Draw the snake */
	DrawSnake(game.drawingArea);	

	/* Draw the fruit */
	DrawFruit(game.drawingArea);

	/* Draw the menu */
	DrawMenu(game.drawingArea);

	/* Show the new surface */
	gtk_widget_queue_draw(game.drawingArea);
}