
#ifndef SNAKE
#define SNAKE

typedef struct Snake Snake;
struct Snake
{
	int length; /* Length of the snake */
	struct sNode *first; /* Pointer to the head of the snake */
	struct sNode *last; /* Pointer to the end of the snake */
	int direction; /* Direction of the snake (0 for north, 1 for east, 2 for south, 3 for west) */
};

typedef struct sNode sNode;
struct sNode
{
	int x;
	int y;
	struct sNode *next; /* Pointer to the next SnakeNode*/
};

typedef struct Fruit Fruit;
struct Fruit
{
	int x;
	int y;
};

Snake * initSnake();

void addSnakeQueueNode(Snake * snake, int x, int y);

void addSnakeHeadNode(Snake * snake, int x, int y);

void deleteSnakeQueueNode(Snake * snake);

void freeSnake(Snake * snake);

void changeSnakeDirection(Snake * snake, int newDiretion);

void snakeDeplacement(Snake * snake, int growth, int oldDirection);

Fruit * initFruit();

void changeFruitPosition(Snake * snake, Fruit * fruit);

#endif