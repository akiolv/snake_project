

CC ?= gcc
OPTIMFLAGS = -O
PKGCONFIG = pkg-config
PACKAGES = gtk+-3.0

WFLAGS = -Wall
CFLAGS = $(OPTIMFLAGS) $(shell $(PKGCONFIG) --cflags $(PACKAGES))
LDFLAGS ?= $(shell $(PKGCONFIG) --libs $(PACKAGES))

SOBJS ?= main.o snake.o graphic.o drawer.o keyboard.o constraints.o

.SUFFIXES: .c .o

SNAKE = snake

all: $(SNAKE)

$(SNAKE): $(SOBJS)
	$(CC) -o $(SNAKE) $(SOBJS) $(CFLAGS) $(LDFLAGS)

.c.o:
	$(CC) -o $*.o -c $*.c $(CFLAGS) $(LDFLAGS) $(WFLAGS)

clean:
	$(RM) *.o *.so *.orig *~ snake core* *.hh.gch
