#include "snake.h"
#include "utils.h"
#include <assert.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * initSnake
 *
 * Function to initialize the Snake with 2 blocs with the head centered  
 */
Snake* initSnake(){
	
	Snake* snake = malloc(sizeof(Snake));

	if (snake == NULL)
	{
		exit(EXIT_FAILURE);
	}

	snake -> length = 0;
	snake -> first = NULL;
	snake -> last = NULL;

	// Create the sNodes
	sNode * first = malloc(sizeof(sNode));
	sNode * end = malloc(sizeof(sNode));

	// Initialize the coordinates of these blocs
	first -> x = m / 2;
	first -> y = m / 2;
	first -> next = end;
	end -> x = m / 2;
	end -> y = (m / 2) + 1;
	end -> next = NULL;

	snake -> length = 2;
	snake -> first = first;
	snake -> last = end;
	snake -> direction = 0;

	return snake;
}


/*
 * addSnakeQueueNode
 *
 * Add a node to the snake at the end
 */
void addSnakeQueueNode(Snake * snake, int x, int y){

	sNode * newNode = malloc(sizeof(sNode));

	if (newNode == NULL)
	{
		exit(EXIT_FAILURE);
	}

	newNode -> x = x;
	newNode -> y = y;
	newNode -> next = NULL;

	sNode * oldNode = snake -> last;
	oldNode -> next = newNode;

	snake -> length += 1;
	snake -> last = newNode;
}


/*
 * addSnakeHeadNode
 *
 * Add a node to the snake at its head
 */
void addSnakeHeadNode(Snake * snake, int x, int y){

	sNode * newNode = malloc(sizeof(sNode));

	if (newNode == NULL)
	{
		exit(EXIT_FAILURE);
	}

	newNode -> x = x;
	newNode -> y = y;
	newNode -> next = snake -> first;

	snake -> length += 1;
	snake -> first = newNode;
}

/*
 * deleteSnakeQueueNode
 *
 * Delete the last node
 */
void deleteSnakeQueueNode(Snake * snake){

	sNode * node = snake -> first;

	for(int i = 0; i < snake -> length - 1; i++){
		node = node -> next;
	}

	free(node -> next);

	node -> next = NULL;

	snake -> last = node;
	snake -> length -= 1;
}


/*
 * freeSnake
 *
 * Free the snake object allocation
 */
void freeSnake(Snake * snake){

	sNode * firstNode = snake -> first;
	sNode * nextNode = firstNode -> next;

	free(firstNode);
	for(int i = 1; i < snake -> length; i++){
		sNode * currentNode = nextNode;
		nextNode = currentNode -> next;
		free(currentNode);
	}

	free(snake);
}


/*
 * changeSnakeDirection
 *
 * Change the snake direction
 * Check if the new direction is valid if:
 *   - |oldDir - newDir| != 2
 */
void changeSnakeDirection(Snake * snake, int newDir){

	int oldDir = snake -> direction;

	if(abs(oldDir - newDir) != 2){
		snake -> direction = newDir;
	}
}


/*
 * snakeDeplacement
 *
 * Deplace the snake. With the arguments:
 *   - growth: 0 if not and 1 if the user reach the fruit
 *   - newDir: new direction for the snake
 */
void snakeDeplacement(Snake * snake, int growth, int newDir){

	changeSnakeDirection(snake, newDir);

	if(growth == 0){
		deleteSnakeQueueNode(snake);
	}

	switch(snake -> direction)
	{
		// North
		case 0:
			addSnakeHeadNode(snake, snake -> first -> x, snake -> first -> y - 1);
			break;

		// East
		case 1:
			addSnakeHeadNode(snake, snake -> first -> x + 1, snake -> first -> y);
			break;

		// South
		case 2:
			addSnakeHeadNode(snake, snake -> first -> x, snake -> first -> y + 1);
			break;

		// West
		case 3:
			addSnakeHeadNode(snake, snake -> first -> x - 1, snake -> first -> y);
			break;
	}
}


/*
 * initFruit
 *
 * Function to initialize the initFruit with 2 coordinates choosen randomly  
 */
Fruit* initFruit(){

	Fruit * fruit = malloc(sizeof(Fruit));

	if (fruit == NULL)
	{
		exit(EXIT_FAILURE);
	}

	srand(time(NULL));
	int x = (rand() % ((game.nbFakePixel - 2) - 1)) + 2;
	int y = (rand() % ((game.nbFakePixel - 2) - 1)) + 2;

	// To avoid the generation of the fruit on the initial snake
	while(x == game.nbFakePixel / 2 || y == game.nbFakePixel / 2|| y == (game.nbFakePixel / 2) + 1){
		x = (rand() % ((game.nbFakePixel - 2) - 1)) + 2;
		y = (rand() % ((game.nbFakePixel - 2) - 1)) + 2;		
	}
 
 	fruit -> x = x;
 	fruit -> y = y;

 	return fruit;
}


/*
 * changeFruitPosition
 *
 * Change the fruit position ramdomly in the window.
 * The fruit can't spawn in the snake
 */

void changeFruitPosition(Snake * snake, Fruit * fruit){

	srand(time(NULL));
	int x;
	int y;
	int validPosition = 1;

	do
	{
		validPosition = 1;

		x = (rand() % ((game.nbFakePixel - 2) - 1)) + 2;
		y = (rand() % ((game.nbFakePixel - 2) - 1)) + 2;
		
		/* Check if the fruit doesn't spawn in the snake */
		sNode * node = snake -> first;
		for(int i = 0; i < snake -> length; i++){
			if(node -> x == x && node -> y == y){
				validPosition = 0;
				continue;
			}
			node = node -> next;
		}

	}
	while(validPosition == 0);

	fruit -> x = x;
	fruit -> y = y;
}