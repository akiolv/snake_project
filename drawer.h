#include <gtk/gtk.h>

#ifndef DRAWER_H_
#define DRAWER_H_

void clearSurface();

gboolean configureEventSurface (GtkWidget *widget, GdkEventConfigure *event, gpointer data);

gboolean drawSurface (GtkWidget *widget, cairo_t *cr, gpointer user_data);
	
void DrawMenuLine(GtkWidget *widget, cairo_t *cr, gpointer user_data);

void DrawGameThreshold(GtkWidget *widget, cairo_t *cr, gpointer user_data);

void DrawGameGrid(GtkWidget *widget);

void DrawMenu(GtkWidget *widget);

void DrawPixel(GtkWidget *widget, cairo_t * cr, int x0, int y0, int fpixel);

void DrawSnake(GtkWidget *widget);

void DrawFruit(GtkWidget *widget);

void DrawGame();

#endif