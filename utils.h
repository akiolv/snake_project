#include <stdlib.h>
#include <stdio.h>
#include <gtk/gtk.h>
#include "snake.h"

#ifndef UTILS
#define UTILS

// Config
#define WINDOW_WIDTH_DEFAULT 1080
#define WINDOW_HEIGTH_DEFAULT 720      // To have a 16:9 format
#define WINDOW_TITLE "Snake"
#define LINE_WIDTH 4

typedef enum
{
	s = 30,
	m = 40,
	l = 90,
	xl = 120,
} nb_pixel_for_game;

struct 
{
	GtkWidget *window;
	GtkWidget *drawingArea;
	cairo_surface_t *surface;

	int nbFakePixel;

	int play;
	int showGrid;
	Snake * snake;
	Fruit * fruit;
} game;


#endif